<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 11.08.2016
 * Time: 19:15
 */

namespace app\models;


class Dictionary
{
    private $words;
    private $eng;
    private $rus;


    /**
     * Dictionary constructor.
     */
    public function __construct()
    {
        $this->words = [
            "apple" => "яблоко",
            "peach" => "персик",
            "orange" => "апельсин",
            "grape" => "виноград",
            "lemon" => "лимон",
            "pineapple" => "ананас",
            "watermelon" => "арбуз",
            "coconut" => "кокос",
            "banana" => "банан",
            "pomelo" => "помело",
            "strawberry" => "клубника",
            "raspberry" => "малина",
            "melon" => "дыня",
            "apricot" => "абрикос",
            "mango" => "манго",
            "plum" => "слива",
            "pomegranate" => "гранат",
            "cherry" => "вишня",
        ];
        $this->wordsRu = [
            "яблоко" => "apple",
            "персик" => "peach",
            "апельсин" => "orange",
            "виноград" => "grape",
            "лимон" => "lemon",
            "ананас" => "pineapple",
            "арбуз" => "watermelon",
            "кокос" => "coconut",
            "банан" => "banana",
            "помело" => "pomelo",
            "клубника" => "strawberry",
            "малина" => "raspberry",
            "дыня" => "melon",
            "абрикос" => "apricot",
            "манго" => "mango",
            "слива" => "plum",
            "гранат" => "pomegranate",
            "вишня" => "cherry",
        ];
        $this->eng = [
            "apple",
            "peach",
            "orange",
            "grape",
            "lemon",
            "pineapple",
            "watermelon",
            "coconut",
            "banana",
            "pomelo",
            "strawberry",
            "raspberry",
            "melon",
            "apricot",
            "mango",
            "plum",
            "pomegranate",
            "cherry",
        ];
        $this->rus = [
            "яблоко",
            "персик",
            "апельсин",
            "виноград",
            "лимон",
            "ананас",
            "арбуз",
            "кокос",
            "банан",
            "помело",
            "клубника",
            "малина",
            "дыня",
            "абрикос",
            "манго",
            "слива",
            "гранат",
            "вишня",
        ];
    }
    
    public function getRandWord($lang = 'en'){
        try{
            if(!empty($this->words) && !empty($this->wordsRu)){
                $rand = rand(0, (count($this->words)-1));
                $index = 0;
                if($lang == 'en'){
                    foreach ($this->words as $key => $word) {
                        if($index === $rand){
                            return [$key => $word];
                        }
                        $index++;
                    }
                }else{
                    foreach ($this->wordsRu as $key => $word) {
                        if($index === $rand){
                            return [$key => $word];
                        }
                        $index++;
                    }
                }
            }else{
                throw new \Exception('Нет слов в словаре!');
            }
        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getRandTest(){
        try{
            if(!empty($this->words) && !empty($this->eng) && !empty($this->rus)){
                $rand = rand(0, 1);
                if($rand === 0){
                    $word = $this->getRandWord('en');
                    $answers = [];
                    for($i=0;$i<4;$i++){
                        $answers[] = $this->rus[rand(0 , (count($this->rus)-1))];
                    }
                    $answers[rand(0,3)] = $word[array_keys($word)[0]];
                    return [
                        'word' => array_keys($word)[0],
                        'answers' => $answers
                    ];
                }else{
                    $word = $this->getRandWord('ru');
                    $answers = [];
                    for($i=0;$i<4;$i++){
                        $answers[] = $this->eng[rand(0 , (count($this->eng)-1))];
                    }
                    $answers[rand(0,3)] = $word[array_keys($word)[0]];
                    return [
                        'word' => array_keys($word)[0],
                        'answers' => $answers
                    ];
                }
            }else{
                throw new \Exception('Нет слов в словаре!');
            }
        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function testAnswer($word, $answer){
        try{
            if(!empty($word) && !empty($answer)){
                if(preg_match('/^[a-zA-Z]+$/', $word)){
                    if(!empty($this->words[mb_strtolower($word)]) && $this->words[mb_strtolower($word)] == mb_strtolower($answer)){
                        return 1;
                    }else{
                        $sql = "
                            INSERT INTO errors (word, answer)
                            VALUES(:word, :answer)
                        ";
                        \Yii::$app->db->createCommand($sql)
                            ->bindValues([':word' => $word, ':answer' => $answer])
                            ->execute();
                        return 0;
                    }
                }else{
                    if(!empty($this->wordsRu[mb_strtolower($word)]) && $this->wordsRu[mb_strtolower($word)] == mb_strtolower($answer)){
                        return 1;
                    }else{
                        $sql = "
                            INSERT INTO errors (word, answer)
                            VALUES(:word, :answer)
                        ";
                        \Yii::$app->db->createCommand($sql)
                            ->bindValues([':word' => $word, ':answer' => $answer])
                            ->execute();
                        return 0;
                    }
                }
            }else{
                throw new \Exception('Не передан параметр!');
            }
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }
}