<?php

namespace app\controllers;

use app\models\Dictionary;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{

    public function beforeAction($action) {
        $this->enableCsrfValidation = ($action->id !== "testanswer" && $action->id !== "gettest" && $action->id !== "savepoints");
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->renderPartial('index');
    }

    public function actionGettest(){
        $dictionary = new Dictionary();
        $q = $dictionary->getRandTest();
        if(!empty($q)){
            echo json_encode($q);
        }
    }

    public function actionTestanswer(){
        if(!empty($_POST)){
            $post = json_decode(array_keys($_POST)[0]);
            $dictionary = new Dictionary();
            $q = $dictionary->testAnswer($post->word, $post->answer);
            echo $q;
        }
    }

    public function actionSavepoints() {
        if(!empty($_POST)){
            $post = json_decode(array_keys($_POST)[0]);
            if(!empty($post->name) && !empty($post->points)){
                try{
                    $sql = "
                        INSERT INTO `points` (name, points)
                        VALUES (:name,:points)
                    ";
                    $q = Yii::$app->db->createCommand($sql)
                        ->bindValues([':name' => $post->name, ':points' => $post->points])
                        ->execute();
                    if(!$q){
                        throw new \Exception('Не удалось записать в БД ошибку!');
                    }else{
                        echo 1;
                    }
                }catch (\Exception $e){
                    echo $e->getMessage();
                }
            }
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
