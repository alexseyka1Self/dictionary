///<reference path="../node_modules/@angular/platform-browser/src/browser.d.ts"/>
import { bootstrap }    from '@angular/platform-browser-dynamic';
import { AppComponent } from './app.component';
import {appRouterProviders} from "./app.routes";
import {HTTP_PROVIDERS} from "@angular/http";

bootstrap(AppComponent, [
  appRouterProviders,
  HTTP_PROVIDERS,
]);
