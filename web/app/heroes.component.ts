import { Component, OnInit } from '@angular/core';
import { HeroService } from './hero.service';
import {Router} from "@angular/router";

@Component({
  selector: 'my-heroes',
  templateUrl: 'app/heroes.component.html',
  styles: [`
    .main {
        width: 96vw;
        text-align: center;
    }
    .main h2 {
        font-weight: bold;
    }
    .main h3 {
        float: right;
        border: 1px solid #607D8B;
        padding: 1rem;
        background: #607D8B;
        color: white;
    }
    .main ul {
        list-style: none;
        width: 100vw;
        padding: 0;
    }
    .main ul li {
        width: 35vw;
        display: inline-block;
        border: 1px solid #009688;
        margin: .5rem;
        padding: 1rem;
        background: #009688;
        color: #fff;
        font-size: 2rem;
        cursor: pointer;
    }
    .main ul li:hover {
        background: #4caf50;
    }
  `]
})

export class HeroesComponent implements OnInit {
  // heroes: Hero[];
    heroes:any = {};
  title = 'Tour of Heroes';
  selectedHero: any;
    points: number;
  private addingHero:any;
  private error:any;
  constructor(
    private router: Router,
    private heroService: HeroService) { }

  onSelect(hero: any) { this.selectedHero = hero; }

  ngOnInit() {
      this.getHeroes();
      this.points = 0;
      localStorage.removeItem('errors');
  }

  getHeroes() {
    this.heroService.getHeroes()
      .then(heroes => this.heroes = heroes);
  }

    testAnswer(elem:any, answer:string){
        if(answer && this.heroes.word){
            let result:any;
            this.heroService.testAnswer(this.heroes.word, answer)
                .then(ans => result = ans)
                .then(ans => {
                    if(ans == '1'){
                        this.points++;
                        this.getHeroes();
                    }else{
                        let errors:number = localStorage.getItem('errors') !== undefined ? localStorage.getItem('errors') : 0;
                        if(errors < 3){
                            errors++;
                            alert('Ошибка! Попробуйте снова!');
                            localStorage.setItem('errors', errors.toString());
                        }else{
                            this.heroService.savePoints(this.points, localStorage.getItem('name'))
                                .then(res => {
                                    if(res == '1'){
                                        let link = ['/'];
                                        localStorage.removeItem('name');
                                        alert('К сожалению, вы проиграли!');
                                        this.router.navigate(link);
                                    }else{
                                        console.log(res);
                                    }
                                });
                        }
                    }
                });
        }else{
            console.log('oh...fuck!', answer, this.heroes);
        }
    }

  /*gotoDetail() {
    this.router.navigate(['/detail', this.selectedHero.id]);
  }

  addHero() {
    this.addingHero = true;
    this.selectedHero = null;
  }

  close(savedHero: Hero) {
    this.addingHero = false;
    if (savedHero) { this.getHeroes(); }
  }*/

  /*deleteHero(hero: Hero, event: any) {
    event.stopPropagation();
    this.heroService
      .delete(hero)
      .then(res => {
        this.heroes = this.heroes.filter(h => h !== hero);
        if (this.selectedHero === hero) { this.selectedHero = null; }
      })
      .catch(error => this.error = error);
  }*/

}

