"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var hero_service_1 = require('./hero.service');
var router_1 = require("@angular/router");
var HeroesComponent = (function () {
    function HeroesComponent(router, heroService) {
        this.router = router;
        this.heroService = heroService;
        // heroes: Hero[];
        this.heroes = {};
        this.title = 'Tour of Heroes';
    }
    HeroesComponent.prototype.onSelect = function (hero) { this.selectedHero = hero; };
    HeroesComponent.prototype.ngOnInit = function () {
        this.getHeroes();
        this.points = 0;
        localStorage.removeItem('errors');
    };
    HeroesComponent.prototype.getHeroes = function () {
        var _this = this;
        this.heroService.getHeroes()
            .then(function (heroes) { return _this.heroes = heroes; });
    };
    HeroesComponent.prototype.testAnswer = function (elem, answer) {
        var _this = this;
        if (answer && this.heroes.word) {
            var result_1;
            this.heroService.testAnswer(this.heroes.word, answer)
                .then(function (ans) { return result_1 = ans; })
                .then(function (ans) {
                if (ans == '1') {
                    _this.points++;
                    _this.getHeroes();
                }
                else {
                    var errors = localStorage.getItem('errors') !== undefined ? localStorage.getItem('errors') : 0;
                    if (errors < 3) {
                        errors++;
                        alert('Ошибка! Попробуйте снова!');
                        localStorage.setItem('errors', errors.toString());
                    }
                    else {
                        _this.heroService.savePoints(_this.points, localStorage.getItem('name'))
                            .then(function (res) {
                            if (res == '1') {
                                var link = ['/'];
                                localStorage.removeItem('name');
                                alert('К сожалению, вы проиграли!');
                                _this.router.navigate(link);
                            }
                            else {
                                console.log(res);
                            }
                        });
                    }
                }
            });
        }
        else {
            console.log('oh...fuck!', answer, this.heroes);
        }
    };
    HeroesComponent = __decorate([
        core_1.Component({
            selector: 'my-heroes',
            templateUrl: 'app/heroes.component.html',
            styles: ["\n    .main {\n        width: 96vw;\n        text-align: center;\n    }\n    .main h2 {\n        font-weight: bold;\n    }\n    .main h3 {\n        float: right;\n        border: 1px solid #607D8B;\n        padding: 1rem;\n        background: #607D8B;\n        color: white;\n    }\n    .main ul {\n        list-style: none;\n        width: 100vw;\n        padding: 0;\n    }\n    .main ul li {\n        width: 35vw;\n        display: inline-block;\n        border: 1px solid #009688;\n        margin: .5rem;\n        padding: 1rem;\n        background: #009688;\n        color: #fff;\n        font-size: 2rem;\n        cursor: pointer;\n    }\n    .main ul li:hover {\n        background: #4caf50;\n    }\n  "]
        }), 
        __metadata('design:paramtypes', [router_1.Router, hero_service_1.HeroService])
    ], HeroesComponent);
    return HeroesComponent;
}());
exports.HeroesComponent = HeroesComponent;
//# sourceMappingURL=heroes.component.js.map