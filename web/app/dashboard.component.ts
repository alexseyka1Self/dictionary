import { Component, OnInit } from '@angular/core';
import { HeroService } from './hero.service';
import { Router } from '@angular/router';

@Component({
  selector: 'my-dashboard',
  templateUrl: 'app/dashboard.component.html',
  styleUrls: ['app/dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  heroes: any = {};

  constructor(
    private router: Router,
    private heroService: HeroService) {
  }

  ngOnInit() {

      if(localStorage.getItem('name').length){
          let link = ['/test'];
          this.router.navigate(link);
      }
  }

  gotoDetail(hero: any) {
    let link = ['/detail', hero.id];
    this.router.navigate(link);
  }

  savename(name:any){
    if(name){
        localStorage.setItem('name',name);
        let link = ['/test'];
        this.router.navigate(link);
    }
  }
}
